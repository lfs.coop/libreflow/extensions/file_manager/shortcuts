import os
import re
from kabaret import flow
from libreflow.utils.kabaret.subprocess_manager.flow import RunAction


class ProductionPathValue(flow.values.SessionValue):

    _action = flow.Parent()
   
    def revert_to_default(self):
        value = self.root().project().get_action_value_store().get_action_value(
            self._action.name(),
            self.name(),
        )
        if not value:
            self.root().project().get_action_value_store().ensure_default_values(
                self._action.name(),
                dict(
                    folder_path="PRODUCTION"
                )
            )
            return self.revert_to_default()
        
        self.set(value)


class OpenProductionFolder(RunAction):

    ICON = ('icons.gui', 'open-folder')

    folder_path = flow.SessionParam('', ProductionPathValue).ui(hidden=True)

    def __init__(self, parent, name):
        super(OpenProductionFolder, self).__init__(parent, name)

    def runner_name_and_tags(self):
        return "DefaultEditor", []

    def extra_argv(self):
        return [f'{self.root().project().get_root()}/{self.folder_path.get()}']

    def needs_dialog(self):
        self.folder_path.revert_to_default()
        
        if not os.path.exists(f'{self.root().project().get_root()}/{self.folder_path.get()}'):
            return True
        return False

    def get_buttons(self):
        self.message.set("<b><font color=red>Cannot find the production folder.</b></font>")
        return ["Cancel"]

    def run(self, button):
        if button == "Cancel":
            return
        
        super(OpenProductionFolder, self).run(button)


def open_production_folder(parent):
    if re.match('^/\D+/synchronization$', parent.oid()) or re.match('.*/tasks/[^/]+$', parent.oid()):
        r = flow.Child(OpenProductionFolder).ui(label='Open Production')
        r.name = 'open_production_folder'
        r.index = None
        return r


def install_extensions(session): 
    return {
        "shortcuts": [
            open_production_folder,
        ],
    }
